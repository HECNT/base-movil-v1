import React, { Component } from 'react';
import axios from 'axios';
import { Camera, Permissions, FileSystem, Notifications } from 'expo';
import Expo from "expo";

import { StyleSheet, View, StatusBar, Alert, TouchableOpacity, ScrollView, CameraRoll, Image, Modal } from 'react-native';
import { StackNavigator, DrawerNavigator } from 'react-navigation';
import { TabNavigator } from 'react-navigation';
import { Ionicons } from '@expo/vector-icons';
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  Tabs,
  Tab,
  Right,
  Left,
  Body,
  Card, CardItem, Text, Content, Fab,  List, ListItem, Spinner
} from "native-base";
import Loading from './component/Loading';
import ctrlServer from '../server/controllers/inicio';
import ctrl from '../client/controllers/inicio';
import helpers from './modules/helpers';

// COMMON JS
var Promise = require('bluebird');

const styles = StyleSheet.create({
  contentContainer: {
    paddingVertical: 20,
    paddingHorizontal : 20
  }
});

class InicioScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading : true,
      colorHeader: "#DF0101",
      msg: "Inicio",
      list: [],
      isModalVisible: false,
      persona: {nombre: "", ap: "", am: "", edad: "", correo: ""}
    };
  }

  async componentWillMount() {

    let aw = await ctrlServer.createTable()

    if (aw.err) {
      Alert.alert('Opss',aw.description,[{text: 'OK', onPress: () => this.props.navigation.navigate("Inicio")}],{ cancelable: false })
      return 0;
    }

    aw = await ctrlServer.getData()

    this.setState({ list: aw.data, loading: false })

  }

  Close = () => {
    this.setState({ isModalVisible : false });
  }

  showModal = (item) => {
    console.log(item,'***********');
    this.setState({ isModalVisible: true, persona: item});
  }

  render() {
    if (this.state.loading) {
      return (
        <Loading colorHeader={this.state.colorHeader} msg={this.state.msg}/>
      );
    } else {

      return (
        <Container style={{marginTop: StatusBar.currentHeight}}>
          <Header style = {{ backgroundColor: this.state.colorHeader }}>
            <Left>
              <Button transparent onPress={() => this.props.navigation.navigate('DrawerOpen')}>
              <Icon name="md-menu" style = {{ color: '#fff' }}/>
              </Button>
            </Left>
            <Body>
              <Title>Crud</Title>
            </Body>
            <Right>

            </Right>
          </Header>
          <Content>
            <ScrollView>
              <View>

                <List dataArray={this.state.list} style={{ flex: 1, backgroundColor: '#fff' }}
                renderRow={(item) =>
                  <ListItem onPress={() => this.showModal(item)}>
                    <Left>
                      <Text>{item.nombre}</Text>
                    </Left>
                    <Right>
                      <Icon name="md-arrow-forward" />
                    </Right>
                  </ListItem>
                }>
                </List>
             </View>
            </ScrollView>
          </Content>

          <Modal visible={this.state.isModalVisible} onRequestClose={() => this.Close()} animationType={'slide'}>
            <Container >
              <Header style = {{ backgroundColor: this.state.colorHeader }}>
                <Left>
                  <Button transparent onPress = {this.Close}>
                  <Icon name="arrow-back" style = {{ color: '#fff' }}/>
                  </Button>
                </Left>
                <Body>
                  <Title>Nombre {this.state.persona.nombre}</Title>
                </Body>
                <Right>
                  <Button transparent onPress={() => this.props.navigation.navigate('Editar')}>
                    <Icon name="md-create" style = {{ color: '#fff' }}/>
                  </Button>
                </Right>
              </Header>
              <Content>
                <ScrollView>
                  <List style={{paddingBottom : 60, flex: 1, backgroundColor: '#fff'}} >
                    <ListItem>
                      <Text>Nombre {this.state.persona.nombre}</Text>
                    </ListItem>
                    <ListItem>
                      <Text>A Paterno {this.state.persona.ap}</Text>
                    </ListItem>
                    <ListItem>
                      <Text>A Materno {this.state.persona.am}</Text>
                    </ListItem>
                    <ListItem>
                      <Text>Edad {this.state.persona.edad}</Text>
                    </ListItem>
                    <ListItem>
                      <Text>Correo {this.state.persona.correo}</Text>
                    </ListItem>
                  </List>
                </ScrollView>
              </Content>
            </Container>
          </Modal>

        </Container>
      );
    }
  }
}

export default InicioScreen;
