import React, { Component } from "react";
import { Image, Button } from "react-native";
import Expo from "expo";
import {
  Content,
  Text,
  List,
  ListItem,
  Icon,
  Container,
  Left,
  Right,
  Badge,
  Spinner,
  View
} from "native-base";
import styles from "./style";

const drawerCover = require("../../../assets/1.jpg");
const drawerImage = require("../../../assets/logo0.png");
const datas = [
  {
    name: "Inicio",
    route: "Inicio",
    icon: "md-home",
    bg: "#C5F442"
  },
  {
    name: "Crud",
    route: "Crud",
    icon: "md-home",
    bg: "#C5F442"
  },
];

class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4,
      loading : true,
      noSesion: true,
    };
  }

  async componentWillMount() {

  }


  render() {
    return (
      <Container>
        <Content
          bounces={false}
          style={{ flex: 1, backgroundColor: "#fff", top: -1 }}
        >
          <Image source={drawerCover} style={styles.drawerCover} />
          <Image square style={styles.drawerImage} source={drawerImage} />

          <List
            dataArray={datas}
            renderRow={data =>
              <ListItem
                button
                noBorder
                onPress={() => this.props.navigation.navigate(data.route)}
              >
              <Icon
                active
                name={data.icon}
                style={{ color: "#777", fontSize: 26, width: 30 }}
              />
                <Left>
                  <Text style={styles.text}>
                    {data.name}
                  </Text>
                </Left>
              </ListItem>}
          />
        </Content>
      </Container>
    );
  }
}

export default SideBar;
