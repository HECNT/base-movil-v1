import React, { Component } from 'react';
import axios from 'axios';
import { Camera, Permissions, FileSystem, Notifications } from 'expo';
import Expo from "expo";

import { StyleSheet, View, StatusBar, Alert, TouchableOpacity, ScrollView, CameraRoll, Image } from 'react-native';
import { StackNavigator, DrawerNavigator } from 'react-navigation';
import { TabNavigator } from 'react-navigation';
import { Ionicons } from '@expo/vector-icons';
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  Tabs,
  Tab,
  Right,
  Left,
  Body,
  Card, CardItem, Text, Content, Fab,  List, ListItem, Spinner
} from "native-base";
import Loading from './component/Loading';
import ctrlServer from '../server/controllers/inicio';
import ctrl from '../client/controllers/inicio';
import helpers from './modules/helpers';

// COMMON JS
var Promise = require('bluebird');

const styles = StyleSheet.create({
  contentContainer: {
    paddingVertical: 20,
    paddingHorizontal : 20
  }
});

class InicioScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      InicioScreen : "",
      loading : true,
      colorHeader: "#DF0101",
      msg: "Inicio",
      res: "",
      axios: false
    };
  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
    });

    // PROBAR SQLITE
    let aw = await ctrlServer.getTest();

    if (aw.err) {
      Alert.alert('Opss',aw.description,[{text: 'OK', onPress: () => this.props.navigation.navigate("Inicio")}],{ cancelable: false })
      return 0;
    }
    this.setState({ loading: false })
  }

  render() {
    if (this.state.loading) {
      return (
        <Loading colorHeader={this.state.colorHeader} msg={this.state.msg}/>
      );
    } else {
      return (
        <Container style={{marginTop: StatusBar.currentHeight}}>
          <Header style = {{ backgroundColor: this.state.colorHeader }}>
            <Left>
              <Button transparent onPress={() => this.props.navigation.navigate('DrawerOpen')}>
              <Icon name="md-menu" style = {{ color: '#fff' }}/>
              </Button>
            </Left>
            <Body>
              <Title>Inicio</Title>
            </Body>
            <Right>

            </Right>
          </Header>
          <Content>
            <ScrollView>
              <View style={{padding:10}}>
                <Text> SQLite activo {this.state.res}</Text>
                <Text> {this.state.axios ? 'Axios activo' : 'Axios err'} </Text>
                <Text> {this.state.location ? 'Localizacion activa' : 'Localizacion err'} </Text>
                <Text> {this.state.internet ? 'Acceso a internet' : 'No tienes acceso a internet'} </Text>
                <Text> {this.state.wifi ? 'Wifi activo' : 'Wifi desactivado'} </Text>
             </View>
            </ScrollView>
          </Content>
        </Container>
      );
    }
  }
}

export default InicioScreen;
