import React, { Component } from 'react';
import axios from 'axios';
import Expo from 'expo';

// var d = {
//   att1: data1,
//   att2: data2,
//   att3: data3,
// }
// axios.get('uri', d, {
//   headers: {
//     Authorization: apiKey
//   }
// })
// .then((res) => {
//   if (res.data.length === 0) {
//     resolve({err: true, description: "No se encontraron cuentas"})
//   } else {
//     resolve({err: false, description: `Se encontrarón ${res.data.length} cuentas`, data: res.data})
//   }
// }).catch((err)=>{
//   resolve({err: true, description:err})
// })

class serviceInicio extends React.Component {

  static getTestService(d){
    return new Promise(function(resolve, reject) {
      axios.get('https://jsonplaceholder.typicode.com/comments')
      .then( (res) => {
        resolve({ err: false, data: res });
      }).catch( (err)=> {
        resolve({ err: true, description:err })
      })
    });;
  }

}

export default serviceInicio;
