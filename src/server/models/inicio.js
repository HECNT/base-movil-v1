import React, { Component } from 'react';
import axios from 'axios';
import Expo from 'expo';

const nameDb = "db.db";
const db = Expo.SQLite.openDatabase(nameDb);

class inicio extends React.Component {

  static getTest(d){
    return new Promise(function(resolve, reject) {
      var query = `
        select 1+1dsadsa as res
      `;
      var inputs = [];
      db.transaction((tx) => {
        tx.executeSql(query, inputs, (tx, result) => {
          resolve({ err: false, res: result })
        }, (err) => {
          resolve({ err: true, error: err })
        })
      })
    });;
  }

  static createTable(d){
    return new Promise(function(resolve, reject) {
      var query = `
        CREATE TABLE IF NOT EXISTS persona (
          persona_id int primary key not null,
          nombre text not null,
          ap text not null,
          am text not null,
          edad int not null,
          correo text not null
        )
      `;
      var inputs = [];
      db.transaction(
        tx => {
          tx.executeSql(query, inputs, (err, { rows }) =>{
            if (err._error != null) {
              resolve({err: true, description: "Hubo un error al tratar de ejecutar el query"})
            } else {
              resolve({err: false})
            }
          });
      },
      null
      );
    });;
  }

  static insertTest(d){
    return new Promise(function(resolve, reject) {
      var query = `
        INSERT INTO
          persona (persona_id, nombre, ap, am, edad, correo)
        VALUES
          (1, 'JOSE OSVALDO', 'HERNANDEZ', 'RITO', 23, 'hecntdev@gmail.com')

      `;
      var inputs = [];
      db.transaction(
        tx => {
          tx.executeSql(query, inputs, (err, { rows }) =>{
            if (err._error != null) {
              resolve({err: true, description: "Hubo un error al tratar de ejecutar el query"})
            } else {
              resolve({err: false})
            }
          });
      },
      null
      );
    });;
  }

  static getData(d){
    return new Promise(function(resolve, reject) {
      var query = `
        SELECT
          *
        FROM
          persona
      `;
      var inputs = [];
      db.transaction(
        tx => {
          tx.executeSql(query, inputs, (err, { rows }) =>{
            if (err._error != null) {
              resolve({err: true, description: "Hubo un error al tratar de ejecutar el query"})
            } else {
              resolve({err: false, data: rows._array})
            }
          });
      },
      null
      );
    });;
  }

}

export default inicio;
