import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { StackNavigator, DrawerNavigator, DrawerView, DrawerItems, SafeAreaView } from 'react-navigation';
import { Root } from "native-base";
import SideBar from "./src/screens/sidebar/";
import InicioScreen from './src/screens/InicioScreen'
import CrudScreen from './src/screens/CrudScreen'
import EditarScreen from './src/screens/EditarScreen'

const Menu = DrawerNavigator({
  Inicio: { screen: InicioScreen },
  Crud: { screen: CrudScreen },
  Editar: { screen: EditarScreen },
},{
  drawerOpenRoute: 'DrawerOpen',
  drawerCloseRoute: 'DrawerClose',
  drawerToggleRoute: 'DrawerToggle',
  contentComponent: props => <SideBar {...props} />
});

const Nav = StackNavigator({
  Menu:       { screen: Menu },
},{
  initialRouteName: "Menu",
  headerMode: "none"
});

export default class App extends React.Component {
  render() {
    return (
      <Root>
        <Nav />
      </Root>
    );
  }
}
